import Vue from 'vue'
import router from './router'
import BoostrapVue from 'bootstrap-vue'
import App from './App.vue'

import 'bootstrap/dist/css/bootstrap.css'

Vue.config.productionTip = false

Vue.use(BoostrapVue)
Vue.use(router)
new Vue({
  el: '#app',
  router: router,
  render: h => h(App)
})

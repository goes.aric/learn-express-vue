const express = require('express')
const app = express()
const port = 3000

const AuthController = require('./controller/AuthController')
app.use('/', AuthController)

app.listen(port, function(){
  console.log('Listening port '+port)
})
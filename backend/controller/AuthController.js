const mysql = require('../config/database')
const express = require('express')
const bcrypt = require('bcrypt')
const cors = require('cors')
const router = express.Router()

router.use(cors())
router.use(express.json())
router.use(express.urlencoded({extended: true}))

router.post('/register', function(req, res) {
  // BCRYPT VARIABLE
  const saltRounds = 10;
  const salt = bcrypt.genSaltSync(saltRounds)
  
  // REGISTER VARIABLE
  let name = req.body.name
  let email = req.body.email
  let username = req.body.username
  let password = bcrypt.hashSync(req.body.password, salt)

  // CREATE AND RUN QUERY
  const query = "INSERT INTO `users` (`name`, `email`, `username`, `password`) VALUES (?, ?, ?, ?)"
  const values = [name, email, username, password]
  mysql.query(query, values, function(err, results){
    if (err) throw  err

    if (results) {
      const response = {
        status: 'success',
        message: 'Register successfully'
      }

      res.json(response)      
    } else {
      const response = {
        status: 'error',
        message: 'Register failed'
      }

      res.json(response)      
    }
  })
})

// LOGIN FUNCTION
router.post('/login', function(req, res){
  // LOGIN VARIABLE
  const username = req.body.username
  const password = req.body.password

  // CREATE AND RUN LOGIN QUERY
  const query = "SELECT * FROM `users` WHERE `username` = ?"
  const values = [username]
  mysql.query(query, values, function(err, results, fields){
    if (err) throw err

    if (bcrypt.compareSync(password, results[0].password)) {
      const response = {
        status: 'success',
        message: 'Login successfully'
      }

      res.json(response)
    } else {
      const response = {
        status: 'error',
        message: 'Username or password are wrong!'
      }

      res.send(response)
    }
  })
})

//LOGOUT FUNCTION
router.get('/logout', function(req, res){
  res.send('logout')
})

module.exports = router